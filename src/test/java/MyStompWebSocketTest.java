import messages.LoginCustomer;
import messages.Message;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.JettyXhrTransport;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MyStompWebSocketTest {

    private static Logger log = LoggerFactory.getLogger(MyStompWebSocketTest.class);

    String endpoint = "login";
    String topic = "app";
    String context = "auth-1.0-SNAPSHOT/";

    @Test
    public void LoginTest() throws Exception {
        String host = "localhost";
        int port = 8080;

        String homeUrl = "http://{host}:{port}/" + context + endpoint;
        log.debug("Sending warm-up HTTP request to " + homeUrl);
        HttpStatus status = new RestTemplate().getForEntity(homeUrl, Void.class, host, port).getStatusCode();
        Assert.state(status == HttpStatus.OK);

        StandardWebSocketClient webSocketClient = new StandardWebSocketClient();

        HttpClient jettyHttpClient = new HttpClient();
        jettyHttpClient.setMaxConnectionsPerDestination(1000);
        jettyHttpClient.setExecutor(new QueuedThreadPool(1000));
        jettyHttpClient.start();

        List<Transport> transports = new ArrayList<>();
        transports.add(new WebSocketTransport(webSocketClient));
        transports.add(new JettyXhrTransport(jettyHttpClient));

        SockJsClient sockJsClient = new SockJsClient(transports);

        try {
            ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
            taskScheduler.afterPropertiesSet();

            String stompUrl = "ws://{host}:{port}/" + context + endpoint;
            WebSocketStompClient stompClient = new WebSocketStompClient(sockJsClient);
            stompClient.setMessageConverter(new MappingJackson2MessageConverter());
            stompClient.setTaskScheduler(taskScheduler);
            stompClient.setDefaultHeartbeat(new long[]{5, 5});

            StompSessionHandlerAdapter handler = new ConsumerStompSessionHandler();
            StompSession session = stompClient.connect(stompUrl, handler, host, port).get();

            for(int i = 0; i < 100; i++) {
                LoginCustomer loginCustomer = new LoginCustomer("fpi@bk.ru", "123123" + (new Random().nextBoolean() ? "" : "typo"));
                log.info(">> {}", loginCustomer);
                session.send("/app/" + endpoint, loginCustomer);
                Thread.sleep(2000);
            }

            System.out.println("\nPress Enter key to exit...");
            System.in.read();
            session.disconnect();
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            jettyHttpClient.stop();
        }

        log.debug("Exiting");
        System.exit(0);
    }

    class ConsumerStompSessionHandler extends StompSessionHandlerAdapter {
        @Override
        public void afterConnected(final StompSession session, StompHeaders connectedHeaders) {
            log.info("connected...");
            session.setAutoReceipt(true);
            session.subscribe("/topic/" + topic, new StompFrameHandler() {
                @Override
                public Type getPayloadType(StompHeaders headers) {
                    return Message.class;
                }

                @Override
                public void handleFrame(StompHeaders headers, Object payload) {
                    log.info("<< {}", payload);
                    Message msg = (Message) payload;
                    Message.Type msgType = Message.Type.valueOf(msg.type);
                    if(msgType == Message.Type.CUSTOMER_API_TOKEN)
                        log.info("Login success: {}", msg.data);
                    else
                        log.error("Login failure: [{}] {}", msgType.name(), msg.data);

                }
            });
        }

        @Override
        public void handleTransportError(StompSession session, Throwable ex) {
            log.error("Transport error: " + ex, ex);
        }

        @Override
        public void handleException(StompSession s, StompCommand c, StompHeaders h, byte[] p, Throwable ex) {
            log.error("Handling exception: " + ex, ex);
        }


    }


}
