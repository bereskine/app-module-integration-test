package model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 12:11
 */
public class ErrorDetails {

    @JsonProperty("error_code")
    public String code;

    @JsonProperty("error_description")
    public String description;

    public ErrorDetails() {
    }

    public ErrorDetails(String code, String description) {
        this.code = code;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ErrorDetails{" +
                "code='" + code + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
