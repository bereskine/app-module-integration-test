package common;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 10:08
 */
public class AppUtils {

    public static final DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter dtfJson = ISODateTimeFormat.dateTime();

    public static String formatDate(long date) {
        return dtf.print(date);
    }

    public static String formatJsonDate(long date) {
        return dtfJson.print(date);
    }

}
