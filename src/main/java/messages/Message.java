package messages;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 12:03
 */
public class Message {

    public String type;

    @JsonProperty("sequence_id")
    public String sequenceId;

    public Object data;

    public enum Type{
        LOGIN_CUSTOMER,
        CUSTOMER_API_TOKEN,
        CUSTOMER_ERROR,
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"{" +
                "type='" + type + '\'' +
                ", sequenceId='" + sequenceId + '\'' +
                ", data=" + data +
                '}';
    }
}
