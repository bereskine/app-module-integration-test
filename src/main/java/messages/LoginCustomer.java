package messages;

import model.UserCredentials;

import java.util.UUID;

/**
 * @author Eugene Bereskin mailto: <a href="mailto:john.boroda@gmail.com">john.boroda@gmail.com</a>
 *         Created: 07.10.2015 12:02
 */
public class LoginCustomer extends Message {

    public LoginCustomer() {
    }

    public LoginCustomer(String email, String password) {
        this.type = Type.LOGIN_CUSTOMER.name();
        this.sequenceId = UUID.randomUUID().toString();
        this.data = new UserCredentials(email, password);
    }

}
